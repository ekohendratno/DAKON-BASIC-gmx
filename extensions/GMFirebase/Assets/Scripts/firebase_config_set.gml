///firebase_config_set(key, value)

/*
basic keys:

    -apiKey
    -authDomain
    -databaseURL
    -projectId
    -storageBucket
    -messagingSenderId
    -emailVerify

*/

global.FIREBASE_config[? argument0] = argument1;


