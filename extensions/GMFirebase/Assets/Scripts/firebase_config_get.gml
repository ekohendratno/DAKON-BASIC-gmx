///firebase_config_get(key)

/*
basic keys:

    -apiKey
    -authDomain
    -databaseURL
    -projectId
    -storageBucket
    -messagingSenderId
    -emailVerify

*/

return global.FIREBASE_config[? argument0];


